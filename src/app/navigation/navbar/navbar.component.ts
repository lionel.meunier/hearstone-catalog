import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { NavbarDisplayService } from '../services/navbar-display.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {

  isCollapsed = true;
  logo = 'assets/angular.png';
  title = 'Mon App';

  navbarDisplay: Observable<boolean>;

  constructor(private navbarDisplayService: NavbarDisplayService) {
  }

  ngOnInit() {

    this.navbarDisplay = this.navbarDisplayService.onChangeDisplay();
  }

}
