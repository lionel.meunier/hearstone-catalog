import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { BtnDisplayNavbarComponent } from './btn-display-navbar/btn-display-navbar.component';
import { NavScrollDirective } from './directives/nav-scroll.directive';
import { UppercasePipe } from './pipes/uppercase.pipe';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [NavbarComponent, BtnDisplayNavbarComponent, NavScrollDirective, UppercasePipe, SearchBarComponent],
  exports: [NavbarComponent, BtnDisplayNavbarComponent, NavScrollDirective],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    NgSelectModule,
    RouterModule
  ]
})
export class NavigationModule { }
