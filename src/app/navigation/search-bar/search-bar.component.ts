import { Component, OnInit } from '@angular/core';
import { Observable, of, Subject, throwError } from 'rxjs';
import { Card } from '../../catalog/interfaces/card.interface';
import { catchError, filter, switchMap, tap } from 'rxjs/operators';
import { CardsService } from '../../catalog/services/cards.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  query = 'default';
  cards$: Observable<Card[]>;
  cardLoading = false;
  query$: Subject<string> = new Subject<string>();
  selectedCard: Card;

  constructor(
    private router: Router,
    private cardsService: CardsService) {
  }

  ngOnInit() {
    this.cards$ = this.query$.pipe(
      tap(t => console.log('ii', t)),
      filter(t => typeof t === 'string'),
      filter(t => t.length > 2),
      tap(() => {
        this.cardLoading = true;
      }),
      switchMap(query => {
        return this.cardsService.search(query);
      }),
      tap(() => {
        this.cardLoading = false;
      }),
      catchError((reason) => {
        this.cardLoading = false;
        return of([]);
      })
    );
  }

  change($event) {
    if ($event) {
      this.router.navigate(['/', 'cards', $event.cardId]);
    }

  }
}
