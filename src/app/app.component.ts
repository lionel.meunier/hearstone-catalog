import { Component, OnInit } from '@angular/core';
import { NavbarDisplayService } from './navigation/services/navbar-display.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'heartstone-catalogu';
  appNavbarDisplay = true;

  constructor(private navbarDisplayService: NavbarDisplayService) {

  }

  ngOnInit(): void {
    this.navbarDisplayService.onChangeDisplay().subscribe((isDisplay) => {
      console.log('test in app', isDisplay);
      this.appNavbarDisplay = isDisplay;
    });
  }


}
