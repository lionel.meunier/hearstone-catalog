import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CardsService } from '../services/cards.service';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { Card } from '../interfaces/card.interface';
import { Observable, of, Subject } from 'rxjs';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  card$: Observable<Card>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private cardsService: CardsService,
    @Inject(DOCUMENT) private document,
  ) {
  }

  ngOnInit() {
    this.card$ = this.activatedRoute.params.pipe(
      map(params => params.id),
      switchMap(id => this.cardsService.getCard(id)),
      switchMap(card => this.loadImage(card)),
      tap(card => console.log(card)),
      catchError(reason => {
        console.log(reason);
        this.router.navigate(['..']);
        return of(null);
      })
    );
  }

  loadImage(card: Card): Observable<Card> {
    console.log(document, card);
    if (card) {
      if (!card.img) {
        card.img = 'http://wow.zamimg.com/images/hearthstone/backs/animated/Card_Back_Troll.gif';
      }
    }
    const subject = new Subject<Card>();
    const img: HTMLImageElement = this.document.createElement('img');
    img.onload = () => {
      subject.next(card);
    };
    img.onerror = () => {
      card.img = 'http://wow.zamimg.com/images/hearthstone/backs/animated/Card_Back_Troll.gif';
      subject.next(card);
    };
    img.src = card.img;
    return subject.pipe();
  }

}
