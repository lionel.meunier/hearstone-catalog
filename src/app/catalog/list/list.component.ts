import { Component, OnInit } from '@angular/core';
import { Card } from '../interfaces/card.interface';
import { BehaviorSubject, merge, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { CardsService } from '../services/cards.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SearchService } from '../services/search.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  cards$: Observable<Card[]>;
  info$: Observable<{ classes: string[], sets: string[] }>;
  filterForm$: BehaviorSubject<{set: string, classe: string}>;

  filterForm = new FormGroup({
    set : new FormControl('Basic',
      [Validators.required]),
    classe : new FormControl('Druid',
      [Validators.required]),
  });

  constructor(
    private cardsService: CardsService,
    private searchService: SearchService
  ) {
  }

  ngOnInit() {
    this.filterForm$ = new BehaviorSubject({
      set : this.filterForm.get('set').value,
      classe :  this.filterForm.get('classe').value
    });
    this.cards$ = merge(
      this.searchService.searchForm$.pipe(
        switchMap(query => {
          return this.cardsService.search(query);
        })
      ),
      this.filterForm$.pipe(
        switchMap(query => {
          return this.cardsService.getCards(query.set, query.classe);
        })
      )
    );
    this.info$ = this.cardsService.info();
  }

  filter() {
    if (this.filterForm.valid) {
      this.filterForm$.next(this.filterForm.getRawValue());
    }
  }
}
